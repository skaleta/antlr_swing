grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog 
    : (stat | block)+ EOF!
    ;

block 
    : LB^ NL! (stat | block)* RB!
    ;

stat
    : expr NL -> expr
    | VAR ID (PODST expr)? NL -> ^(VAR ID) ^(PODST ID expr)?
    | ID PODST expr NL -> ^(PODST ID expr)
    | while_stat NL -> while_stat    
    | NL ->
    ;

expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      )*
    ;
    
while_expr
    : expr
      ( EQ^ expr
      | NEQ^ expr
      )*
    ;

multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
	  
while_stat
    : WHILE^ LP! while_expr RP! NL? block
    ;	  

  
VAR 
    : 'var'
    ;

WHILE
    : 'while'
    ;
    
  
LB 
	  : '{'
	  ; 
  
RB
	  : '}'
	  ;
  
EQ
	  : '=='
	  ;
  
NEQ
	  : '!='
	  ;
  
ID 
    : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;

INT 
    : '0'..'9'+
    ;

NL 
    : '\r'? '\n' 
    ;

WS 
    : (' ' | '\t')+ {$channel = HIDDEN;} 
    ;

LP
    : '('
    ;

RP
    : ')'
    ;

PODST
    : '='
    ;

PLUS
    : '+'
    ;

MINUS
    : '-'
    ;

MUL
    : '*'
    ;

DIV
    : '/'
    ;
  