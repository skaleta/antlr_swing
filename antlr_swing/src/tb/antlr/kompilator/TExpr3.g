tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer number = 0;
  Integer lable_nr = 0;
}
prog      : (e+=comm_block | e+=expr | e+=cond | d+=decl)* -> program(name={$e},declarations={$d})
;

comm_block : ^(LB {enterScope();} (e+=comm_block | e+=expr | e+=cond | d+=decl)* {leaveScope();}) -> block(statement={$e},declaration={$d})
;

decl      : ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> declare(p1={$ID.text})
;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr      : ^(PLUS  e1=expr e2=expr) -> plus(p1={$e1.st},p2={$e2.st})
	        | ^(MINUS e1=expr e2=expr) -> minus(p1={$e1.st},p2={$e2.st})
	        | ^(MUL   e1=expr e2=expr) -> mul(p1={$e1.st},p2={$e2.st})
	        | ^(DIV   e1=expr e2=expr) -> div(p1={$e1.st},p2={$e2.st})
	        | ^(PODST i1=ID   e2=expr) {locals.hasSymbol($i1.text);} -> assign(p1={$ID},p2={$e2.st})
	        | INT                      -> int(i={$INT.text})
	        | ID                       {locals.checkSymbol($ID.text);} -> id(i={$ID.text})
	        | ^(WHILE stat=cond command=comm_block )  -> while(stat={$stat.st},command={$command.st},nr={lable_nr.toString()})	       
;        

cond      :  ^(EQ    e1=expr e2=expr) {++lable_nr;} -> isEqual(p1={$e1.st},p2={$e2.st},nr={lable_nr.toString()})
	        | ^(NEQ    e1=expr e2=expr) {++lable_nr;} -> isNotEqual(p1={$e1.st},p2={$e2.st},nr={lable_nr.toString()})
;
    